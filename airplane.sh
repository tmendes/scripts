#!/bin/sh

BT=$(bluetooth | grep off)
WF=$(wifi | grep off)

echo -n " "
if [[ -n $BT ]] && [[ -n $WF ]]; then
    echo "On"
else
    echo "Off";
fi
