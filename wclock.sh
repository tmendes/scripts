#!/bin/sh

WEATHER=~/Library/Applications/scripts/weather.sh
DATEFORMAT="+%d/%m/%y - %H:%M"

SP=`env TZ=Ameria/Sao_Paulo date "$DATEFORMAT"`
NY=`env TZ=America/New_York date "$DATEFORMAT"`
PE=`env TZ=America/Lima date "$DATEFORMAT"`
KR=`env TZ=Europe/Warsaw date "$DATEFORMAT"`
NZ=`env TZ=Pacific/Auckland date "$DATEFORMAT"`
BL=`env TZ=Europe/Minsk date "$DATEFORMAT"`

echo -e "* Krakow       $KR - PL"
echo -e "* Campinas     $SP - BR"
echo -e "* Minsk        $BL - BL"
echo -e "* Auckland     $NZ - NZ"
echo -e "* New York     $NY - US"
echo -e "* Lima         $PE - PE"
