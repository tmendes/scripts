#!/bin/sh

EMOJI=1
BAT_WARNING=15

if [ $EMOJI -eq 1 ]; then
	HEART_FULL="❤️"
	HEART_EMPTY="💔"
	CHARCHING="⚡️"
	DISCHARGING="🕯️"
	WARNING="😭"
else
	HEART_FULL=♥
	HEART_EMPTY=♡
	CHARCHING="+"
	DISCHARGING="-"
	WARNING="W"
fi

_print_battery_hearts()
{
	local heart_bar
	local charge_bar
	local bar_size=5
	local inc=$(( 100 / $bar_size))
	local acpi="$(acpi --battery 2>/dev/null)"
	local power="${acpi#Battery *, }"

	power="${power%%%*}"

	if [[ "$acpi" == *"Discharging"* ]]; then
		if [ $power -lt $BAT_WARNING ]; then
			charge_bar=$WARNING
		else
			charge_bar=$DISCHARGING
		fi
	else
		charge_bar=$CHARCHING
	fi

	for idx in `seq $bar_size`; do
		local perc=`expr $idx \* $inc - $inc / 2`
		if [ $power -lt $perc ]; then
			heart_bar="$HEART_EMPTY""$heart_bar"
		else
			heart_bar="$HEART_FULL""$heart_bar"
		fi
	done

	echo $heart_bar" "$power"% "$charge_bar
}

_print_battery_hearts
